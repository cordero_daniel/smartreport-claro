<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * Get the StudentForm that owns the Vote.
     */
    public function studentForm()
    {
        return $this->belongsTo('App\StudentForm');
    }
}
