<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * Get the StudentForm that owns the file.
     */
    public function studentForm()
    {
        return $this->belongsTo('App\StudentForm');
    }
}
