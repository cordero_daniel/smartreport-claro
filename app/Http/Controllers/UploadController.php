<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use App\StudentForm;

class UploadController extends Controller {

    public function upload(Request $request) {
        if ($request->hasFile('fl_video') && $request->hasFile('fl_ficha')) {
            if ($request->file('fl_video')->isValid() && $request->file('fl_ficha')->isValid()) {
                
                $this->validate($request, [
                    'txt_nombre_completo' => 'string|required',
                    'txt_phone' => 'string|required',
                    'txt_universidad' => 'string|required',
                    'txt_carne' => 'string|required|unique:student_forms,identification_number',
                    'txt_email' => 'string|required|unique:student_forms,email',
                    'fl_video' => 'max:100000'
                ], [
                    'txt_carne.unique' => "Ya hay un registro correspondiente a este número de Carné",
                    'txt_email.unique' => "Ya hay registro correspondiente a este correo electrónico",
                    'fl_video' => "El video no puede pesar más de 100Mb"
                ]);
                $studentForm = new StudentForm();
                $studentForm->name = $request->get('txt_nombre_completo');
                $studentForm->last_name = 'N/A';
                $studentForm->phone_number = $request->get('txt_phone');
                $studentForm->identification_number = str_slug($request->get('txt_carne'), '-');
                $studentForm->email = $request->get('txt_email');
                $studentForm->university = $request->get('txt_universidad');
                $studentForm->save();
                
                $video = $request->file('fl_video');
                $videoFile = new File();
                $videoFile->size = $video->getClientSize();
                $videoFile->name = 'video-' . str_slug($studentForm->identification_number, '-');
                $videoFile->display_name = 'N/A';
                $videoFile->extension = $video->extension();
                $videoFile->type = "video";
                $videoPath = $video->storeAs('videos', $studentForm->identification_number.'.'.$videoFile->extension, ['disk' => 'uploads']);
                $videoFile->absolute_path = public_path() . '/uploads/' . $videoPath;
                $videoFile->web_path = '/uploads/'. $videoPath;
                
                $image = $request->file('fl_ficha');
                $imageFile = new File();
                $imageFile->size = $image->getClientSize();
                $imageFile->name = 'image-' . str_slug($studentForm->identification_number, '-');
                $imageFile->display_name = 'N/A';
                $imageFile->extension = $image->extension();
                $imageFile->type = "image";
                $imagePath = $image->storeAs('images', $studentForm->identification_number.'.'.$imageFile->extension, ['disk' => 'uploads']);
                $imageFile->absolute_path = public_path() . '/uploads/' . $imagePath;
                $imageFile->web_path = '/uploads/'. $imagePath;
                
                $studentForm->files()->saveMany([$videoFile, $imageFile]);
                
                return redirect()->route('thanks', ["id" => $studentForm->id]);
                
            }
        }
    }

}
