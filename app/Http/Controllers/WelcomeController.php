<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentForm;
use App\Vote;
use App\Options;

class WelcomeController extends Controller {

    public function index(Request $request){
        $newsStudents = StudentForm::where('approved', true)->get()->sortByDesc('id')->take(3);
        $studentForms = StudentForm::where('approved', true)->get();
        $studentForms = $studentForms->diff($newsStudents);
        $totalPages = count($studentForms) / 3;
        $totalPages = ceil($totalPages);
        $voting = (Options::where('name', 'show_voting')->first()->value == "true") ? true : false;
        $votes = Vote::all();
        
        $votesNewsStudent = array();
        foreach ($newsStudents as $newsStudentForm){
            $count = 0;
            foreach($votes as $vote){
                if($vote->student_form_id == $newsStudentForm->id){
                    $count++;
                }
            }
            $votesNewsStudent[$newsStudentForm->id] = $count;
        }
        
        $votesStudent = array();
        foreach ($studentForms as $studentForm){
            $count = 0;
            foreach($votes as $vote){
                if($vote->student_form_id == $studentForm->id){
                    $count++;
                }
            }
            $votesStudent[$studentForm->id] = $count;
        }
        
        return view('welcome', ['studentForms' => $studentForms, 'newsStudents' => $newsStudents, 'totalPages' => $totalPages, 'voting' => $voting, 'votesNewsStudent' => $votesNewsStudent, "votesStudent" => $votesStudent]);
    }
    
    /**
     * Vote one participant.
     *
     * @return \Illuminate\Http\Response
     */
    public function vote(Request $request, $id)
    {
        $ip = $request->ip();
        $vote = Vote::where('ip_address', $ip)->first();
        if($vote == null){
            $studentForm = StudentForm::where('id', $id)->first();
            $vote = new Vote();
            $vote->ip_address = $ip;
            $vote->studentForm()->associate($studentForm);
            $vote->save();
        }
        
        return redirect()->route('welcome');
    }
    
    /**
     * Show one participant.
     *
     * @return \Illuminate\Http\Response
     */
    public function participant(Request $request, $id)
    {
        $studentForm = StudentForm::where("id", $id)->first();
        return view('participant', ['studentForm' => $studentForm]);
    }
    
    /**
     * Show thanks for one participant.
     *
     * @return \Illuminate\Http\Response
     */
    public function thanks(Request $request, $id)
    {
        $studentForm = StudentForm::where("id", $id)->first();
        return view('thanks', ['studentForm' => $studentForm]);
    }

}
