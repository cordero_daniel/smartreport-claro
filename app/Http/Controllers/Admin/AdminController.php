<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\StudentForm;

class AdminController extends Controller {

    public function __construct() {
        
    }

    /**
     * Show the participants list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        $participants = StudentForm::paginate(10);

        return view('admin.participants', ['participants' => $participants]);
    }
    
    /**
     * Approve one participant.
     *
     * @return \Illuminate\Http\Response
     */
    public function approveParticipant(Request $request) {
        $studentForm = StudentForm::where('id', $request->get('_participant'))->first();
        
        $errors = array();
        if($studentForm != null){
            $studentForm->approved = ($request->get('approved') == 'on') ? true : false;
            $studentForm->save();
            $message = ($request->get('approved') == 'on') ? 'Formulario Aprobado!' : 'Formulario Desaprobado!';
        }else{
            array_push($errors, 'No se ha encontrado el participante!');
            return redirect()->route('admin_index')->withErrors($errors);
        }

        return redirect()->route('admin_index')->with(['status' => $message, 'errors' => $errors]);
    }

}
