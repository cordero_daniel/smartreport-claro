<?php

use Illuminate\Database\Seeder;
use App\Options;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $option = new Options();
        $option->name = "show_voting";
        $option->value = "false";
        $option->type = "boolean";
        
        $option->save();
    }
}
