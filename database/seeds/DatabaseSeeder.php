<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->email = "admin@admin.com";
        $admin->password = bcrypt("admin");
        $admin->name = "Admin";
        
        $admin->save();
    }
}
