$(document).ready(function(){
    
    $('.modal-caller').click(function(){
        $link = $($(this).children()[0]);
        
        //Carga del video
        $('#video-source').attr("src", $link.data('video'));
        $('.embed-responsive-item')[0].load();
        
        //Carga de la imagen
        $('#image-source').attr("src", $link.data('image'));
        
        //Id participante en formulario
        $('#_participant').val($link.data('participant'));
        
        if($link.data('approved') == '1'){
            $('#approved').prop('checked', true);
        }else{
            $('#approved').prop('checked', false);
        }
        
        $('.modal-title').text($link.data('fullname'));
        
        $('#main-modal').on('hidden.bs.modal', function () {
            $('#video-source').attr("src", "");
            $('.embed-responsive-item')[0].load();
        })
        
    });
    
});

