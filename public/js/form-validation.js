// Wait for the DOM to be ready
$(function() {
	// Initialize form validation on the registration form.
	$('#btn_enviar').on('click', function() {
		// It has the name attribute "registration"
		$("form[name='smartregform']").validate({
			// Specify validation rules
			rules : {
				// The key name on the left side is the name attribute
				// of an input field. Validation rules are defined
				// on the right side
				txt_nombre_completo : "required",
				txt_phone : "required",
				txt_email : {
					required : true,
					// Specify that email should be validated
					// by the built-in "email" rule
					email : true
				},
				txt_universidad : "required",
				txt_carne : "required",
				fl_video : {
					required : true,
					accept : "video/mp4",
					filesize : 104857600,//100MB
				},
				fl_ficha : {
					required : true,
					extension : "png|jpg"
				}
			},
			// Specify validation error messages
			messages : {
				txt_nombre_completo : "Por favor ingrese su nombre y apellido",
				txt_phone : "Por favor ingrese su número de teléfono",
				txt_email : "Por favor ingrese una dirección de correo válida",
				txt_universidad : "Por favor ingrese el nombre de su Universidad",
				txt_carne : "Por favor ingrese el número de su carné de estudiante",
				fl_video : "Por favor adjunte un archivo video .MP4 y menor a 100MB",
				fl_ficha : "Por favor adjunte la ficha técnica"
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler : function(form) {
				form.submit();
			}
		});
	});

	//only letters on nombrecompleto
	$('#txt_nombre_completo').keypress(function(e) {
		var regex = new RegExp("^[a-zA-Z\b\\s]$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});
	//only numbers on telefono
	$(".number").keydown(function(e) {

		if (event.shiftKey) {
			event.preventDefault();
		}

		if (event.keyCode == 46 || event.keyCode == 8) {

		} else {
			if (event.keyCode < 95) {
				if (event.keyCode < 48 || event.keyCode > 57) {
					event.preventDefault();
				}
			} else {
				if (event.keyCode < 96 || event.keyCode > 105) {
					event.preventDefault();
				}
			}
		}
	});
	$.validator.addMethod('filesize',function(value, element, param){
		//param = size (bytes)
		//element = element to validate (<input>)
		//value =  value of the element (file name)
		
		return this.optional(element) || (element.files[0].size <= param);
	});
});

