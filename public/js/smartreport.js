/**
 * @author Daniel.Cordero
 */

$('#nav-toggle').click(function() {
	$(this).toggleClass('expanded').siblings('div').slideToggle();
});

$('.participa').on('click', function() {
	$('#smartregform').slideToggle(400);
});

$('#more-videos').on('click', function() {
	$('#full-gallery').slideToggle(400);
});

$('#universidades').on('click', function() {
	$('.logos-universidades').slideToggle(400);
});

$('#presentan').on('click', function() {
	$('.logos-presentan').slideToggle(400);
});

$('#apoyan').on('click', function() {
	$('.logos-apoyan').slideToggle(400);
});
$('#patrocinan').on('click', function() {
	$('.logos-patrocinan').slideToggle(400);
});
$('#jurados').on('click', function() {
	$('#jurados-section').slideToggle(400);
});
$('#ganadoresfinales').on('click', function() {
	$('#ganadores-finales-section').slideToggle(400);
});

/*
 Listeners Jurados, when clicked it will display their respective, photo and BIO
 * */
$('#damian').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/jurados.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items
			/*
			 *  json['jurados'][0] = the first jurado (damian) on the Jurados array
			 *  json['jurados'][0]['name'] =  gets prop value Damian García
			 *  json['jurados'][0]['bio'] =  gets Damian garcia's Bio
			 *  json['jurados'][0]['picture'] = gets damian garci's photo
			 */

			$('#modalJurado .jurado-info .bio').html('<p><strong>' + json['jurados'][0]['name'] + '</strong></p>' + '<br/>' + '<p>' + json['jurados'][0]['bio'] + '</p>' + '<br/>');
			$('#modalJurado .jurado-info .foto').html('<img src=' + json['jurados'][0]['picture'] + ' class=\'img-responsive\' \'/>' + '<br/>');
		}
	});

	/*var bio, photo;

	 bio = "";
	 photo = "";

	 bio = "<p><strong>Damián García</strong></p>" + "<br/>" + "<p>Damián García es máster en Periodismo y Movimientos Sociales egresado de la Universidad Rey Juan Carlos, posee un posgrado en Periodismo de la Universidad de Almería, cursó su  licenciatura en Ciencias de la Información en  la  Universidad Complutense de Madrid. Tiene una amplia experiencia  en desarrollo de contenido, ya que ha trabajado como coordinador de contenido en la Revista Foco Sur, se desempeñó como responsable de comunicación y vicepresidente de contenido en La Máquina de Ideas,  y actualmente se desempeña como director editorial de ND Medios.</p>" + "<br/>";
	 photo = "<img src=\"{{ URL::asset('images/jurados/damian-garcia.jpg') }}\" class='img-responsive' />";

	 $('#modalJurado .jurado-info .bio').html(bio);
	 $('#modalJurado .jurado-info .foto').html(photo);*/

});

$('#eveling').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/jurados.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items
			$('#modalJurado .jurado-info .bio').html('<p><strong>' + json['jurados'][1]['name'] + '</strong></p>' + '<br/>' + '<p>' + json['jurados'][1]['bio'] + '</p>' + '<br/>');
			$('#modalJurado .jurado-info .foto').html('<img src=' + json['jurados'][1]['picture'] + ' class=\'img-responsive\' \'/>' + '<br/>');
		}
	});

});

$('#sara').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/jurados.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items
			$('#modalJurado .jurado-info .bio').html('<p><strong>' + json['jurados'][2]['name'] + '</strong></p>' + '<br/>' + '<p>' + json['jurados'][2]['bio'] + '</p>' + '<br/>');
			$('#modalJurado .jurado-info .foto').html('<img src=' + json['jurados'][2]['picture'] + ' class=\'img-responsive\' \'/>' + '<br/>');
		}
	});

});
$('#arnulfo').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/jurados.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalJurado .jurado-info .bio').html('<p><strong>' + json['jurados'][3]['name'] + '</strong></p>' + '<br/>' + '<p>' + json['jurados'][3]['bio'] + '</p>' + '<br/>');
			$('#modalJurado .jurado-info .foto').html('<img src=' + json['jurados'][3]['picture'] + ' class=\'img-responsive\' \'/>' + '<br/>');
		}
	});

});

/*
 Listeners Ganadores, when clicked it will display their respective, photo and BIO
 * */
$('#luis_sandoval').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items
			/*
			 *  json['jurados'][0] = the first jurado (damian) on the Jurados array
			 *  json['jurados'][0]['name'] =  gets prop value Damian García
			 *  json['jurados'][0]['bio'] =  gets Damian garcia's Bio
			 *  json['jurados'][0]['picture'] = gets damian garci's photo
			 */

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][0]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][0]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][0]['video']);
		}
	});
});
$('#mario_vega').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][1]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][1]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][1]['video']);
		}
	});
});
$('#keira_icaza').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][2]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][2]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][2]['video']);
		}
	});
});
$('#maria_delia').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][3]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][3]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][3]['video']);
		}
	});
});
$('#silvie_chavez').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][4]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][4]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][4]['video']);
		}
	});
});
$('#joana_ortiz').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][5]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][5]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][5]['video']);
		}
	});
});
$('#daniela_bello').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][6]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][6]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][6]['video']);
		}
	});
});
$('#andrea_suarez').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][7]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][7]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][7]['video']);
		}
	});
});
$('#jdavid_gonzales').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][8]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][8]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][8]['video']);
		}
	});
});
$('#yaricksa_espinales').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadores .jurado-info .bio').html('<p><strong>' + json['ganadores'][9]['name'] + '</strong></p>' + '<br/>');
			$('#modalGanadores .jurado-info div video.vjs-tech').attr('src', json['ganadores'][9]['video']);
			$('#modalGanadores .jurado-info video source#video-ganador').attr('src', json['ganadores'][9]['video']);
		}
	});
});

/*
 Listeners Ganadores Finales, when clicked it will display their respective, photo and BIO
 * */
$('#luis_sandoval_fin').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores_finales.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items
			/*
			 *  json['jurados'][0] = the first jurado (damian) on the Jurados array
			 *  json['jurados'][0]['name'] =  gets prop value Damian García
			 *  json['jurados'][0]['bio'] =  gets Damian garcia's Bio
			 *  json['jurados'][0]['picture'] = gets damian garci's photo
			 */

			$('#modalGanadoresFinales .modal-title').html(json['ganadores_finales'][0]['name']);
			$('#modalGanadoresFinales .jurado-info .bio').html('<p>' + json['ganadores_finales'][0]['detail'] + '</p>' + '<br/>');
			$('#modalGanadoresFinales .jurado-info div video.vjs-tech').attr('src', json['ganadores_finales'][0]['video']);
			$('#modalGanadoresFinales .jurado-info video source#video-ganador').attr('src', json['ganadores_finales'][0]['video']);
		}
	});
});

$('#keira_icaza_fin').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores_finales.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadoresFinales .modal-title').html(json['ganadores_finales'][1]['name']);
			$('#modalGanadoresFinales .jurado-info .bio').html('<p>' + json['ganadores_finales'][1]['detail'] + '</p>' + '<br/>');
			$('#modalGanadoresFinales .jurado-info div video.vjs-tech').attr('src', json['ganadores_finales'][1]['video']);
			$('#modalGanadoresFinales .jurado-info video source#video-ganador').attr('src', json['ganadores_finales'][1]['video']);
		}
	});
});
$('#maria_delia_fin').click(function() {

	//start ajax request
	$.ajax({
		url : 'js/ganadores_finales.json',
		//force to handle it as text
		dataType : 'text',
		success : function(data) {
			//data downloaded so we call parseJSON function
			//and pass downloaded data
			var json = JSON.parse(data);

			//now json variable contains data in json format
			//let's display a few items

			$('#modalGanadoresFinales .modal-title').html(json['ganadores_finales'][2]['name']);
			$('#modalGanadoresFinales .jurado-info .bio').html('<p>' + json['ganadores_finales'][2]['detail'] + '</p>' + '<br/>');
			$('#modalGanadoresFinales .jurado-info div video.vjs-tech').attr('src', json['ganadores_finales'][2]['video']);
			$('#modalGanadoresFinales .jurado-info video source#video-ganador').attr('src', json['ganadores_finales'][2]['video']);
		}
	});
});

$collection = $('.errors').find('input');

$.each($collection, function(index, element) {
	alert($(element).val());
});
