/**
 * @author Daniel.Cordero
 */

$('#nav-toggle').click(function() {
  $(this).toggleClass('expanded').siblings('div').slideToggle();
});

$('.participa').on('click', function(){
	$('#smartregform').slideToggle(400);
});

$('#more-videos').on('click',function(){
	$('#full-gallery').slideToggle(400);
});
