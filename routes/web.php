<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::post('upload', 'UploadController@upload')->name('form_submit');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
    
    //Admin Routes
    Route::match(['get', 'post'], '/', 'AdminController@index')->name('admin_index');
    Route::post('/studentForm/aprove', 'AdminController@approveParticipant')->name('admin_approve_participant');
        
    
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/vote/{id}', 'WelcomeController@vote');
Route::get('/participant/{id}', 'WelcomeController@participant')->name('participant');
Route::get('/thanks/{id}', 'WelcomeController@thanks')->name('thanks');
