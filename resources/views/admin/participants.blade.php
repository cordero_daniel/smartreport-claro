@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @foreach($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> {{ $error }}
            </div>
            @endforeach
            @if (session('status'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session('status') }}
            </div>
            @endif
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            Lista de Participantes
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        @foreach ($participants as $participant)
                        <li role="presentation"><a href="#{{ $participant->id }}" data-toggle="modal" data-target="#{{ $participant->id }}">{{ $participant->email }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="panel-footer">
                    {{ $participants->links() }}
                </div>
            </div>
        </div>
    </div>
    @foreach ($participants as $participant)
    <div id="{{ $participant->id }}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id='edit-participant-{{ $participant->id }}' action="{{ route('admin_approve_participant') }}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{ $participant->name }} {{ $participant->last_name }}</h4>
                    </div>
                    <div class="modal-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div align="center" class="embed-responsive embed-responsive-16by9">
                                    @if($participant->files->where('type', 'video')->first() !== null)
                                        <video class="embed-responsive-item" controls>
                                            <source src="{{ URL::asset( $participant->files->where('type', 'video')->first()->web_path ) }}" type=video/mp4>
                                        </video>
                                    @endif
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div align="center">
                                    @if($participant->files->where('type', 'image')->first() !== null)
                                        <img src="{{ URL::asset( $participant->files->where('type', 'image')->first()->web_path ) }}" class="img-responsive" alt="Formulario Físico">
                                    @endif
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        @if($participant->approved)
                                        <input form="edit-participant-{{ $participant->id }}" id="approved" name="approved" type="checkbox" checked="checked">
                                        @else
                                        <input form="edit-participant-{{ $participant->id }}" id="approved" name="approved" type="checkbox">
                                        @endif
                                    </span>
                                    <input type="text" class="form-control" value="Aprobado" readonly>
                                </div><!-- /input-group -->
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button form="edit-participant-{{ $participant->id }}" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                    <input type="hidden" value="{{ $participant->id }}" name="_participant" />
                    <input type="hidden" value="{{ Session::token() }}" name="_token" />
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endforeach
</div>
@endsection
