<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="{{ URL::asset('images/favicon.ico') }}">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="{{ URL::asset('js/jquery.bootpag.min.js') }}" type="text/javascript" charset="utf-8"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<link rel="stylesheet" href="{{ URL::asset('css/reset.css') }}" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="{{ URL::asset('css/scrolling-nav.css') }}" type="text/css" charset="utf-8"/>
		<link href="http://vjs.zencdn.net/5.16.0/video-js.css" rel="stylesheet">
		<link rel="stylesheet" href="{{ URL::asset('css/smartreport_style.css') }}" type="text/css" charset="utf-8"/>
		<!-- Google Analytics -->
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
			ga('create', 'UA-93108636-1', 'auto');
			ga('send', 'pageview');
		</script>

	</head>
	<body>
		<div class="errors">
			@if (count($errors) > 0)
			@foreach ($errors->all() as $error)
			<input type="hidden" value="{{ $error }}" />
			@endforeach
			@endif
		</div>
		<div class="main-container">
			<a href="" id="page-header"></a>
			<div class="mega-menu container">
				<div class="brand-logo col-md-2">
					<a href="#page-header" class="page-scroll"> <img src="{{ URL::asset('images/icon_logo.png') }}" alt="Logo Claro" class="img-responsive brand-icon" /> <img src="{{ URL::asset('images/4Glite_LOGO.png') }}" alt="4G LTE" class="img-responsive lte" /></a>
				</div>
				<nav class="desktop col-md-10">

					<a href="#steps" class="page-scroll">Pasos</a>
					<a href="#awards" class="page-scroll">Premios</a>
					<a href="#gallery" class="page-scroll">Noticias Participantes</a>
					<a href="#jurados-section" class="page-scroll" id="jurados">Jurados</a>
					<a href="#ganadores-finales-section" class="page-scroll" id="ganadoresfinales">Ganadores Finales</a>
				</nav>

				<nav class="navbar navbar-fixed-top mobile">
					<button id="nav-toggle">
						Toggle
					</button>
					<div>

						<a href="#steps" class="page-scroll">Pasos</a>
						<a href="#awards" class="page-scroll">Premios</a>
						<a href="#gallery" class="page-scroll">Noticias Participantes</a>
						<a href="#jurados-section" class="page-scroll" id="jurados">Jurados</a>
						<a href="#ganadores-finales-section" class="page-scroll" id="ganadoresfinales">Ganadores Finales</a>
					</div>
				</nav>
			</div>

			<div class="header">

				<header class="hero-wrapper">
					<div class="row">
						<img src="{{ URL::asset('images/UE_1_01.gif') }}" alt="" class="img-responsive hero-logo ue-hero col-sm-1"/>
						<img src="{{ URL::asset('images/logo.png') }}" alt="" class="img-responsive hero-logo col-sm-6"/>
						<img src="{{ URL::asset('images/logo_claro_white.png') }}" alt="" class="img-responsive hero-logo claro-white col-sm-1"/>
					</div>
				</header>
			</div>

			<div class="body-content">
				<div class="parallax-bg">
					<div class="lead-content">
						<div class="container-fluid">
							<a href="#smart-regform" class="participa CTA page-scroll">Ganadores Primera Fase</a>

							<h1>Llevá el futuro de tu carrera de la mano con lo último de la tecnología</h1>
						</div>

					</div>
				</div>

				<div class="mid-content" id="steps">
					<div class="container-fluid">
						<h2>Participá en la dinámica</h2>
						<div class="row">
							<div class="col-xs-12 col-md-4 ">
								<div class="steps">
									<img src="{{ URL::asset('images/paso1.png') }}" alt="" class="step-img" />
									<p class="step-detail">
										Inscribí el trabajo en tu facultad llenando una ficha técnica
									</p>
								</div>

							</div>
							<div class="col-xs-12 col-md-4 ">
								<div class="steps">
									<img src="{{ URL::asset('images/paso2.png') }}" alt="" class="step-img" />
									<p class="step-detail">
										Subí tu trabajo a la dirección web <a href="http://www.smartreportclaro.com/">www.smartreportclaro.com</a>
									</p>
								</div>

							</div>
							<div class="col-xs-12 col-md-4 ">
								<div class="steps">
									<img src="{{ URL::asset('images/paso3.png') }}" alt="" class="step-img" />
									<p class="step-detail">
										Recordá que no se aceptarán trabajos en disco o memoria, ya que el jurado evaluará a través de www.smartreportclaro.com
									</p>
								</div>

							</div>
						</div>

					</div>

					<a href="#smart-regform" class="participa CTA page-scroll">Ganadores Primera Fase</a>
					<div id="smart-regform" class="regform-wrapper container-fluid">

						<form id="smartregform" name="smartregform" action="{{ route('form_submit') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
							<div class="jurados-container">
								<h2>Conocé a los Ganadores de la Primera Fase</h2>
								<br />
								<div class="row">
									<h3>Elegidos por el Jurado:</h3>
									<br />
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="luis_sandoval" >Luis Sandoval</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="mario_vega" >Mario Vega</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="keira_icaza">Keira Icaza</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="maria_delia"> María Delia Estrada</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="silvie_chavez" >Silvie Chávez</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="joana_ortiz" >Joana Ortiz</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="daniela_bello">Daniela Bello</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="andrea_suarez"> Andrea Suarez</a>
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="jdavid_gonzales" >Jose David Gonzales</a>

									<!--elegida por mayoria de votos-->
									<h3>Elegida por el público:</h3>
									<br />
									<a href="#modalGanadores" data-toggle="modal" data-target="#modalGanadores" id="yaricksa_espinales" >Yaricksa Espinales</a>

								</div>

							</div>

							<!--
							<div >
							<label for="txt_nombre_completo"> Nombre: </label>
							<input type="text" name="txt_nombre_completo" value="" id="txt_nombre_completo" placeholder="Nombre y Apellido" required/>

							</div>

							<div >
							<label for="txt_phone">Teléfono: </label>
							<input type="text" name="txt_phone" value="" id="txt_phone" placeholder="########" maxlength="8" class="number" required/>
							</div>

							<div>
							<label for="txt_email">Correo Eléctrónico: </label>
							<input type="text" name="txt_email" value="" id="txt_email" placeholder="ejemplo@ejemplo.com" required/>
							</div>

							<div>
							<label for="txt_universidad">Universidad: </label>
							<input type="text" name="txt_universidad" value="" id="txt_universidad" placeholder="Universidad Ejemplo" required/>
							</div>

							<div>
							<label for="txt_carne">Carné Universitario: </label>
							<input type="text" name="txt_carne" value="" id="txt_carne" required/>
							</div>
							<div>
							<label for="">Video: <span style="font-size: 12px; color: #848484;">( Máx 100MB )</span></label>
							<input id="fl_video" name="fl_video" type="file" accept=".mp4" value="Seleccionar Archivo" required/>
							</div>
							<div>
							<label for="">Ficha Técnica: </label>
							<input id="fl_ficha" name="fl_ficha" type="file" accept="image/*" value="Seleccionar Archivo" required/>
							</div>
							<div>
							<input type="submit" name="btn_enviar" value="Enviar" id="btn_enviar" class="CTA"/>
							</div>

							{{ csrf_field() }}-->

						</form>

					</div>
					<!--Ganadores Finales-->
					<div id="ganadores-finales-section" class="regform-wrapper container-fluid">

						<div class="jurados-container">
								<h2>Conocé a los Ganadores Finales</h2>
								<br />
								<div class="row">			
									<a href="#modalGanadoresFinales" data-toggle="modal" data-target="#modalGanadoresFinales" id="luis_sandoval_fin" >1er Lugar &mdash; Luis Sandoval</a>
									<a href="#modalGanadoresFinales" data-toggle="modal" data-target="#modalGanadoresFinales" id="maria_delia_fin">2do Lugar &mdash; María Delia Estrada</a>
									<a href="#modalGanadoresFinales" data-toggle="modal" data-target="#modalGanadoresFinales" id="keira_icaza_fin">3er Lugar &mdash; Keira Icaza</a>

								</div>

							</div>
						</form>
					</div>

					<a href="{{ URL::asset('SmartReport-reglamento-smart-report-2017.pdf') }}" class="reglamento" target="_blank">Descargá el reglamento de la dinámica</a>
					<br />
					<a href="{{ URL::asset('Smartreport-ficha-de-inscripcion-smart-report-claro.pdf') }}" class="reglamento" target="_blank">Descargá la ficha técnica acá</a>
				</div>
				<div class="awards" id="awards">
					<div class="container-fluid">
						<h2>Apurate a participar</h2>
						<div class="row">
							<div class="col-xs-12 col-md-6 fase-uno">
								<div class="circulo">
									Primera Fase
								</div>
								<div class="rectangulo">
									10 Ganadores del Taller de Periodismo de Mochila
								</div>
							</div>
							<div class="col-xs-12 col-md-6 fase-dos">
								<div class="circulo">
									Segunda Fase
								</div>
								<div class="rectangulo">
									De los 10 ganadores se seleccionarán 3 primeros lugares
								</div>
							</div>
							<div class="col-xs-12 detalle-premios">

								<div class="col-xs-12 lugar">
									<div class="col-md-1"><img src="{{ URL::asset('images/premio.png') }}" alt="" />
									</div>
									<div class="col-md-11">
										<p>
											Primer Lugar se llevará un Smartphone, un kit de periodismo de mochila, una pasantía de 3 meses en ND Medios, más dinero en efectivo ($300), taller coaching de marca personal en redes sociales impartido por TecnoTool
										</p>
									</div>

								</div>
								<div class="col-xs-12 lugar">
									<div class="col-md-1"><img src="{{ URL::asset('images/premio.png') }}" alt="" />
									</div>
									<div class="col-md-11">
										<p>
											Segundo Lugar será premiado con un smartphone, además de dinero en efectivo($200), taller coaching de marca personal en redes sociales impartido por TecnoTool
										</p>
									</div>

								</div>
								<div class="col-xs-12 lugar">
									<div class="col-md-1"><img src="{{ URL::asset('images/premio.png') }}" alt="" />
									</div>
									<div class="col-md-11">
										<p>
											Tercer Lugar se llevará un smartphone y dinero en efectivo ($100).
										</p>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>
				<div id="gallery" class="videos">
					<div class="container-fluid">
						<h2>Noticias Participantes</h2>
						<div class="videos-container">
							<div class="row">
								@foreach($newsStudents as $newsStudent)
								<div class="col-xs-12 col-md-4 video-thumbnail">
									@if($newsStudent->files->where('type', 'video')->first() !== null)
									<video width="360" class="video-js"  poster="" controls data-setup="{}" preload="none">
										<source src='{{ URL::asset($newsStudent->files->where('type', 'video')->first()->web_path) }}' type="video/mp4">
									</video>
									@endif
									@if($voting)
									<br>
									<a href="vote/{{ $newsStudent->id }}" class="CTA">Votar</a>
									@endif
									<span>{{ $votesNewsStudent[$newsStudent->id] }}</span>
									<br>
									<span class="nombre-participante">Por: {{ $newsStudent->name }}</span>
								</div>
								@endforeach
							</div>
						</div>
					</div>
					<a href="#full-gallery" class="CTA page-scroll" id="more-videos">Ver Más</a>

					<div id="full-gallery">
						<div class="container-fluid">
							<div id="videos-container" class="row">
								@for ($page = 1; $page <= $totalPages; $page++)
								@if($page != 1)
								<div class="row" id="page-{{ $page }}" style="display: none;">
									@else
									<div class="row" id="page-{{ $page }}">
										@endif
										@for ($position = $page * 3 - 2; $position <= $page * 3; $position++)
										@if($studentForms->has($position-1))
										<div class="col-xs-12 col-md-4 video-thumbnail">
											@if($studentForms[$position-1]->files->where('type', 'video')->first() !== null)
											<video width="360" class="video-js video-item"  poster="" controls data-setup="{}" preload="none">
												<source src='{{ URL::asset($studentForms[$position-1]->files->where('type', 'video')->first()->web_path) }}' type="video/mp4">
											</video>
											@endif
											@if($voting)
											<br>
											<a href="vote/{{ $studentForms[$position-1]->id }}" class="CTA">Votar</a>
											@endif
											<span>{{ $votesStudent[$studentForms[$position-1]->id] }}</span>
											<br>
											<span class="nombre-participante">Por: {{ $studentForms[$position-1]->name }}</span>
										</div>
										@endif
										@endfor
									</div>
									@endfor
								</div>
							</div>
							<div id="page-selection">
								<!--Paginator-->
								&nbsp;
							</div>

						</div>

					</div>
					<div id="jurados-section">
						<div class="jurados-container">
							<h2>Conocé al Jurado</h2>
							<div class="row">
								<a href="#modalJurado" data-toggle="modal" data-target="#modalJurado" id="damian" >Damián García</a>
								<a href="#modalJurado" data-toggle="modal" data-target="#modalJurado" id="eveling" >Eveling Lambert</a>
								<a href="#modalJurado" data-toggle="modal" data-target="#modalJurado" id="sara">Sara Avilés</a>
								<a href="#modalJurado" data-toggle="modal" data-target="#modalJurado" id="arnulfo"> Arnulfo Peralta</a>
							</div>

						</div>
					</div>

				</div>

				<footer>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 logos-wrapper">
								<a href="#participantes" id="universidades" class="link-patrocinadores page-scroll">Los Participantes</a>
								<br />
								<div class="logos-universidades" id="participantes">

									<img src="{{ URL::asset('images/logo-uam.png') }}" alt="UAM" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-uca.png') }}" alt="UCA" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-ucc.png') }}" alt="UCC" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-udem.png') }}" alt="UDEM" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-unan.png') }}" alt="UNAN" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-univalle.png') }}" alt="Universidad del Valle" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-uhispam.png') }}" alt="Universidad Hispanoamericana" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-american-college.png') }}" alt="Universidad Hispanoamericana" class="img-responsive" />

								</div>
								<a href="#logos-presenta" id="presentan" class="link-patrocinadores page-scroll">Presentan</a>
								<br />
								<div class="logos-presentan" id="logos-presenta">
									<!-- claro y UE -->
									<img src="{{ URL::asset('images/UE_1_01.gif') }}" alt="Unión Europea" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-claro-presenta.png') }}" alt="Claro Presenta" class="img-responsive" />
								</div>
								<a href="#logos-apoya" id="apoyan" class="link-patrocinadores page-scroll">Apoyan</a>
								<br />
								<div class="logos-apoyan" id="logos-apoya">
									<!--nuevo diario-->
									<img src="{{ URL::asset('images/logo-nuevo-diario.png') }}" alt="" class="img-responsive" />
									<!--q' hubo-->
									<img src="{{ URL::asset('images/logo-q-hubo.png') }}" alt="" class="img-responsive" />
									<!-- nyx tecnotool -->
									<img src="{{ URL::asset('images/logo-nyx.png') }}" alt="" class="img-responsive" />
									<img src="{{ URL::asset('images/logo-tecnotool.png') }}" alt="" class="img-responsive" />
								</div>
								<a href="#logos-patrocina" id="patrocinan" class="link-patrocinadores page-scroll">Patrocinan</a>
								<br />
								<div class="logos-patrocinan" id="logos-patrocina">
									<!--Metro-->
									<img src="{{ URL::asset('images/logo-metro.png') }}" alt="" class="img-responsive" />

								</div>
							</div>

							<div class="col-md-6 copyright">
								<p>
									Todos los derechos reservados, Claro 2017
								</p>
							</div>
						</div>
					</div>

				</footer>

				<!-- modal jurados-->
				<div class="modal fade" id="modalJurado" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									&times;
								</button>
								<h3 class="modal-title">Jurado Calificador</h3>
							</div>
							<div class="modal-body">
								<div class="container-fluid">

									<div class="jurado-info row">
										<div class="bio col-sm-6">

										</div>
										<div class="foto col-sm-6">

										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Cerrar
								</button>
							</div>
						</div>

					</div>
				</div>

				<!--Modal Ganadores-->
				<div class="modal fade" id="modalGanadores" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close cerrar-modal" data-dismiss="modal">
									&times;
								</button>
								<h3 class="modal-title">Ganadores</h3>
							</div>
							<div class="modal-body">
								<div class="container-fluid">

									<div class="jurado-info row">
										<div class="bio">

										</div>

										<div class="video-thumbnail ganadores" >
											<video width="360" class="video-js video-item"  poster="" controls data-setup="{}" preload="none">
												<source id="video-ganador" src="mov_bbb.mp4" type="video/mp4">
											</video>

										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default cerrar-modal" data-dismiss="modal">
									Cerrar
								</button>
							</div>
						</div>

					</div>
				</div>
				
				<!--Modal Ganadores Finales-->
				<div class="modal fade" id="modalGanadoresFinales" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close cerrar-modal" data-dismiss="modal">
									&times;
								</button>
								<h3 class="modal-title">Ganadores</h3>
							</div>
							<div class="modal-body">
								<div class="container-fluid">

									<div class="jurado-info row">
										<div class="bio">

										</div>

										<div class="video-thumbnail ganadores" >
											<video width="360" class="video-js video-item"  poster="" controls data-setup="{}" preload="none">
												<source id="video-ganador" src="mov_bbb.mp4" type="video/mp4">
											</video>

										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default cerrar-modal" data-dismiss="modal">
									Cerrar
								</button>
							</div>
						</div>

					</div>
				</div>
				

			</div>

			<!--Plugins-->
			<script src="{{ URL::asset('js/jquery.easing.min.js') }}" type="text/javascript" charset="utf-8"></script>
			<script src="{{ URL::asset('js/scrolling-nav.js') }}" type="text/javascript" charset="utf-8"></script>
			<script src="{{ URL::asset('js/jquery.validate.min.js') }}" type="text/javascript" charset="utf-8"></script>
			<script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
			<script src="http://vjs.zencdn.net/5.16.0/video.js"></script>

			<!--JS-->
			<script src="{{ URL::asset('js/smartreport.js') }}" type="text/javascript" charset="utf-8"></script>
			<script src="{{ URL::asset('js/form-validation.js') }}" type="text/javascript" charset="utf-8"></script>

			<script>
				$('#page-selection').bootpag({
				total: {{ $totalPages }}, page: 1,
				maxVisible: 5
				}).on("page", function(event, num) {
					event.preventDefault();
					$collectionOfPages = $("#videos-container .row");
					$.each($collectionOfPages, function(index, element) {
						$(element).css('display', 'none');
					});
					$('#page-' + num).css('display', 'block');
				});

				$('.cerrar-modal').click(function() {
					$.fn.stopVideo();
				});

				$.fn.stopVideo = function() {
					$('video').each(function() {
						$(this)[0].pause()
					});
				}

			</script>
	</body>
</html>
