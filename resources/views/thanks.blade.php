<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Smartreport Claro | Claro Nicaragua</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <script src="{{ URL::asset('js/jquery.bootpag.min.js') }}" type="text/javascript" charset="utf-8"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!--CSS-->
        <link rel="stylesheet" href="{{ URL::asset('css/reset.css') }}" type="text/css" charset="utf-8"/>
        <link rel="stylesheet" href="{{ URL::asset('css/scrolling-nav.css') }}" type="text/css" charset="utf-8"/>
        <link href="http://vjs.zencdn.net/5.16.0/video-js.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL::asset('css/smartreport_style.css') }}" type="text/css" charset="utf-8"/>
    </head>

    <body>
        <div class="main-container">
            <a href="" id="page-header"></a>

            <div class="header" style="margin-top: 0;">

                <header class="hero-wrapper">
                    <div class="row">
						<img src="{{ URL::asset('images/UE_1_01.gif') }}" alt="" class="img-responsive hero-logo ue-hero col-sm-1"/>
						<img src="{{ URL::asset('images/logo.png') }}" alt="" class="img-responsive hero-logo col-sm-6"/>
						<img src="{{ URL::asset('images/logo_claro_white.png') }}" alt="" class="img-responsive hero-logo claro-white col-sm-1"/>
					</div>
                </header>
            </div>

            <div class="body-content">			
                <div class="mid-content" id="steps">
                    <div class="container-fluid">
                        <h2>Muchas gracias por Inscribirte quedas participando!</h2>
                        <h3>Comparte el link de tu video en tus redes sociales</h3>
                        <br />
                        <p><a href="{{ route('participant', ['id' => $studentForm->id]) }}" class="video-link">{{ route('participant', ['id' => $studentForm->id]) }}</a></p>
                    </div>		
                </div>
            </div>
            <footer>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 logos-wrapper">
                            <a href="#participantes" id="universidades" class="link-patrocinadores page-scroll">Los Participantes</a>
                            <br />
                            <div class="logos-universidades" id="participantes">

                                <img src="{{ URL::asset('images/logo-uam.png') }}" alt="UAM" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-uca.png') }}" alt="UCA" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-ucc.png') }}" alt="UCC" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-udem.png') }}" alt="UDEM" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-unan.png') }}" alt="UNAN" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-univalle.png') }}" alt="Universidad del Valle" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-uhispam.png') }}" alt="Universidad Hispanoamericana" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-american-college.png') }}" alt="Universidad Hispanoamericana" class="img-responsive" />

                            </div>
                            <a href="#logos-presenta" id="presentan" class="link-patrocinadores page-scroll">Presentan</a>
                            <br />
                            <div class="logos-presentan" id="logos-presenta">
                                <!-- claro y UE -->
                                <img src="{{ URL::asset('images/images/UE_1_01.gif') }}" alt="Unión Europea" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-claro-presenta.png') }}" alt="Claro Presenta" class="img-responsive" />
                            </div>
                            <a href="#logos-apoya" id="apoyan" class="link-patrocinadores page-scroll">Apoyan</a>
                            <br />
                            <div class="logos-apoyan" id="logos-apoya">
                                <!--nd medios-->
                                <img src="{{ URL::asset('images/logo-nd-medios.png') }}" alt="" class="img-responsive" />
                            </div>
                            <a href="#logos-patrocina" id="patrocinan" class="link-patrocinadores page-scroll">Patrocinan</a>
                            <br />
                            <div class="logos-patrocinan" id="logos-patrocina">
                                <!-- nyx tecnotool -->
                                <img src="{{ URL::asset('images/logo-nyx.png') }}" alt="" class="img-responsive" />
                                <img src="{{ URL::asset('images/logo-tecnotool.png') }}" alt="" class="img-responsive" />
                            </div>
                        </div>

                        <div class="col-md-6 copyright">
                            <p>
                                Todos los derechos reservados, Claro 2017
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>


        <!--Plugins-->
        <script src="{{ URL::asset('js/jquery.easing.min.js') }}" type="text/javascript" charset="utf-8"></script>
        <script src="{{ URL::asset('js/scrolling-nav.js') }}" type="text/javascript" charset="utf-8"></script>
        <script src="{{ URL::asset('js/jquery.validate.min.js') }}" type="text/javascript" charset="utf-8"></script>
        <script src="{{ URL::asset('js/additional-methods.min.js') }}"></script>
        <script src="http://vjs.zencdn.net/5.16.0/video.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!--JS-->
        <script src="{{ URL::asset('js/smartreport.js') }}" type="text/javascript" charset="utf-8"></script>
        <script src="{{ URL::asset('js/form-validation.js') }}" type="text/javascript" charset="utf-8"></script>

    </body>
</html>


